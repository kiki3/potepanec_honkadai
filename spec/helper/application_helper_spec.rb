# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it 'returns the title of web page when no argument is given ' do
      expect(helper.full_title).to eq('BIGBAG Store')
    end

    it 'returns the title of web page and a product name when a product name is given' do
      expect(helper.full_title('RUBY ON RAILS BAG')).to eq('RUBY ON RAILS BAG - BIGBAG Store')
    end
  end
end
