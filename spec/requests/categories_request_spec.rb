# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Category_request', type: :request do
  describe 'GET /potepan/categories/:id' do
    let!(:taxonomy) { create(:taxonomy, name: 't') }
    let!(:taxon) { create(:taxon, name: 't1', parent: taxonomy.root) }
    let!(:taxon2) { create(:taxon, name: 't2', parent: taxonomy.root) }
    let!(:product) { create(:product, name: 'Rails Bag', taxons: [taxon]) }
    let!(:product2) { create(:product, name: 'Rails Mug', taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'returns 200 response' do
      expect(response).to have_http_status(200)
    end

    it 'shows a list of categories' do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include taxon.name
      expect(response.body).to include taxon2.name
    end

    it 'shows products of a category' do
      expect(response.body).to include product.name
      expect(response.body).to include product2.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product2.display_price.to_s
    end
  end
end
