# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: 't') }
  given!(:taxon) { create(:taxon, name: 't1', parent: taxonomy.root) }
  given!(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario '商品詳細ページにアクセスする' do
    expect(page).to have_title "#{product.name} - BIGBAG Store"

    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).to have_content product.description
    end
  end

  scenario '商品詳細ページからトップページにアクセスする' do
    within '.lightSection' do
      click_link 'Home'
    end
    expect(page).to have_title 'BIGBAG Store'
  end
end
