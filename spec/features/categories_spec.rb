# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: 't') }
  given!(:taxon) { create(:taxon, name: 't1', parent: taxonomy.root) }
  given!(:product) { create(:product, name: 'Rails Bag', taxons: [taxon]) }
  given!(:product2) { create(:product, name: 'Rails Mug', taxons: [taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario '商品カテゴリーページにアクセスする' do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"

    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(taxon.products.count).to eq(2)
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  scenario '特定のカテゴリの商品を表示する' do
    within all('.productCaption')[0] do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
    within all('.productCaption')[1] do
      expect(page).to have_content product2.name
      expect(page).to have_content product2.display_price
    end
  end

  scenario '商品詳細ページへのアクセスできる' do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
